// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use cached::proc_macro::{cached, once};
use mwbot::parsoid::ImmutableWikicode;
use mwbot::Bot;
use rocket::fairing::AdHoc;
use rocket::http::hyper::header::ACCESS_CONTROL_ALLOW_ORIGIN;
use rocket::http::Header;
use rocket::serde::json::Json;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::BTreeSet;
use toolforge::db::Cluster;
use toolforge::pool::WikiPool;
use wikipedia_prosesize::{parsoid_stylesheet, ProseSize};

const USER_AGENT: &str = toolforge::user_agent!("prosesize");

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[cached(result = true, sync_writes = true)]
async fn bot(domain: String) -> anyhow::Result<Bot> {
    let bot = Bot::builder(format!("https://{domain}/w/"))
        .set_user_agent(USER_AGENT.to_string())
        .build()
        .await?;
    Ok(bot)
}

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    title: Option<String>,
    domain: String,
    revision: Option<u64>,
    wikis: BTreeSet<String>,
    size: Option<ProseSize>,
    html: Option<String>,
}

#[once(sync_writes = true, result = true, time = 86400)]
async fn all_wikis(pool: &WikiPool) -> anyhow::Result<BTreeSet<String>> {
    Ok(pool.list_domains().await?)
}

/// Handle all GET requests for the "/" route. We return either a `Template`
/// instance, or a `Template` instance with a specific HTTP status code.
///
/// We ask Rocket to give us the managed State (see <https://rocket.rs/v0.5-rc/guide/state/#managed-state>)
/// of the database connection pool we set up below.
#[get("/?<title>&<domain>&<revision>")]
async fn index(
    pool: &State<WikiPool>,
    title: Option<&str>,
    domain: Option<&str>,
    revision: Option<u64>,
) -> Result<Template> {
    let domain = domain.unwrap_or("en.wikipedia.org");
    let title = match title {
        Some(title) => title,
        None => {
            let wikis = all_wikis(pool).await?;
            return Ok(Template::render(
                "index",
                IndexTemplate {
                    title: None,
                    domain: domain.to_string(),
                    revision,
                    wikis,
                    size: None,
                    html: None,
                },
            ));
        }
    };
    let (html, wikis) = lookup(pool, domain, title, revision).await?;
    Ok(Template::render(
        "index",
        IndexTemplate {
            title: Some(title.to_string()),
            domain: domain.to_string(),
            revision,
            wikis,
            size: Some(wikipedia_prosesize::prosesize(html.clone())),
            html: Some(fixup_html(html.html())),
        },
    ))
}

async fn lookup(
    pool: &WikiPool,
    domain: &str,
    title: &str,
    revision: Option<u64>,
) -> anyhow::Result<(ImmutableWikicode, BTreeSet<String>)> {
    let wikis = all_wikis(pool).await?;
    let domain = if wikis.contains(domain) {
        domain
    } else {
        "en.wikipedia.org"
    };
    let bot = bot(domain.to_string()).await?;
    let page = bot.page(title)?;
    let html = match revision {
        Some(revision) => page.revision_html(revision).await?,
        None => page.html().await?,
    };
    Ok((html, wikis))
}

fn fixup_html(html: &str) -> String {
    html.replace(
        "</head>",
        &format!("<style>{}</style>", parsoid_stylesheet()),
    )
}

#[get("/api/<domain>/<title>?<revision>")]
async fn api(
    pool: &State<WikiPool>,
    domain: &str,
    title: &str,
    revision: Option<u64>,
) -> Result<Json<ProseSize>, Json<ErrorTemplate>> {
    match lookup(pool, domain, title, revision).await {
        Ok((html, _)) => Ok(Json(wikipedia_prosesize::prosesize(html))),
        Err(err) => Err(Json(ErrorTemplate {
            error: Some(err.to_string()),
            code: 500,
            reason: "Internal Server Error",
        })),
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(
            WikiPool::new(Cluster::WEB)
                .expect("failed to load database config"),
        )
        .mount("/", routes![index, api])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
        .attach(AdHoc::on_response("CORS", |req, resp| {
            Box::pin(async move {
                if req.uri().path().as_str().starts_with("/api/") {
                    resp.set_header(Header::new(
                        ACCESS_CONTROL_ALLOW_ORIGIN.as_str(),
                        "*",
                    ));
                }
            })
        }))
}
